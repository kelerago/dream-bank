export const environment = {
  production: true,
  apiUrl: 'https://api.mockbank.io',
  loginAuthorization: 'Basic MjE0OjIxNA==',
  storageKey: 'dream_bank',
  clientID: 'db195e77-15cb-4ff3-a2e3-3c89c5c2c0bf',
  pageSize: 10
};