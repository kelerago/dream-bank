import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap} from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from '../services/notification.service';

/**
 * Verify if session has expired
 */
@Injectable()
export class ExpiredSessionInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationService
  ){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req)
    .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {             
           
            
          }
        }, error => {
              
            if(error.status == 401 && this.authService.isLoggedIn()) {

                this.authService.logout();

                this.notificationService.error('La sesión ha expirado');
                
                this.router.navigate(['/login']);
            }              

        })
      )
  }
}