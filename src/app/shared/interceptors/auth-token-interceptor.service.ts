import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from "../services/auth.service";

/**
 * Add User Token to Every Request
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let request;

    if(this.authService.isLoggedIn()){

        const userToken = this.authService.getAccessToken();

        request = req.clone({ 
          headers: req.headers.set('Authorization', userToken),
        });

    } else {

        request = req;
    }

    return next.handle(request);
  }
}