import { HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthData } from "src/app/modules/auth/models/auth-data.model";
import { ApplicationHttpClient } from "./application-http-client.service";
import { environment } from "src/environments/environment";
import { Observable, Observer, BehaviorSubject } from "rxjs";

@Injectable()
export class AuthService {

    public authenticationState = new BehaviorSubject(false);

    constructor(
        private httpClient: ApplicationHttpClient
    ) { }

    /**
     * Verify if has a user authentication
     * @returns {boolean}
     */
    public isLoggedIn(): boolean {

        let response;
        
        const authData = localStorage.getItem(environment.storageKey);

        if(authData) {

            response = true;            

        } else{

            response = false;
        }

        this.authenticationState.next(response);

        return response;
    }

    /**     
     * User Login
     * @param {AuthData} authData user data 
     * @returns {Observable}
     */
     public login(authData: AuthData): Observable<any> {

        return Observable.create( (observer: Observer<any>) => {

            let options = {
                headers:  new HttpHeaders(),
                params: new HttpParams()
            } 
    
            options.headers = options.headers.set('Authorization', environment.loginAuthorization)
                             .set('Content-Type', 'application/x-www-form-urlencoded'); 
          
            Object.keys(authData).forEach(key => {
                
                // @ts-ignore: Unreachable code error
                options.params = options.params.set(key, authData[key])
            }); 
    
            return this.httpClient.Post('/oauth/token', {}, options)
            .subscribe(
                (data:any) => {

                    let { access_token, username } = data;

                    localStorage.setItem(environment.storageKey, JSON.stringify({ access_token, username }));

                    this.authenticationState.next(true);        
                    
                    observer.next("");
                    
                },
                () => {

                    observer.error('User or password is incorrect');
                },
                () => {

                    observer.complete();
                }
            );    
        });
    }

    /**
     * Remove user data
     */
    public logout() {

        localStorage.removeItem(environment.storageKey);

        this.authenticationState.next(false);
    }

    public getAccessToken() {

        let response;

        const authData = localStorage.getItem(environment.storageKey);

        if(authData) {

            let token = JSON.parse(authData).access_token;

            response = `Bearer ${ token }`;

        } else{

            response = ''
        }

        return response;
    }
}