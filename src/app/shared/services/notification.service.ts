import { Injectable } from "@angular/core";
import { ToastrService } from 'ngx-toastr';

@Injectable()

export class NotificationService {

    constructor(
        private toastr: ToastrService
    ) {}

    /**
     * Success toastr 
     * @param {String} message message 
     * @param {String} title Title
     */
    success(message: string, title: string  = '') {

        this.toastr.success(message, title);
    }

    /**
     * Error toastr 
     * @param {String} message message 
     * @param {String} title Title
     */
    error(message: string, title: string  = '') {

        this.toastr.error(message, title);
    }
}