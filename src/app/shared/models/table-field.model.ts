export interface TableField {
    value: string,
    text: string
}