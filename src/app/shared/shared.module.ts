import { NgModule } from "@angular/core";
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "../app-routing.module";
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from "@angular/material/core";
import { CommonModule } from "@angular/common";

import { AuthService, NotificationService, LoaderService, ApplicationHttpClient, applicationHttpClientCreator } from "./services";

import { CardComponent, GeneralInformationComponent, TableComponent, LoaderComponent, NavComponent } from './components';

import { AuthGuard, NoAuthGuard } from "./guards";

import { TokenInterceptor, LoaderInterceptor, ExpiredSessionInterceptor } from "./interceptors";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        AppRoutingModule,
        ToastrModule.forRoot(),
        FormsModule,
        LayoutModule, 
        MatToolbarModule, 
        MatButtonModule, 
        MatSidenavModule, 
        MatIconModule, 
        MatListModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatGridListModule,
        MatTableModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatDatepickerModule
    ],
    declarations: [
        NavComponent, 
        CardComponent, 
        GeneralInformationComponent, 
        TableComponent, 
        LoaderComponent
    ],
    providers: [
        AuthService,
        NotificationService,
        LoaderService,
        AuthGuard,
        NoAuthGuard,
        {
            provide: ApplicationHttpClient,
            useFactory: applicationHttpClientCreator,
            deps: [HttpClient]
        },
        {
            provide: HTTP_INTERCEPTORS, 
            useClass: TokenInterceptor, 
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS, 
            useClass: ExpiredSessionInterceptor, 
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS, 
            useClass: LoaderInterceptor, 
            multi: true
        }
    ],
    exports: [
        FormsModule, 
        MatButtonModule,
        MatInputModule, 
        MatGridListModule,
        MatTableModule,
        MatIconModule,
        MatFormFieldModule,
        MatProgressBarModule,
        MatSelectModule,
        MatToolbarModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatNativeDateModule,
        AppRoutingModule,
        NavComponent, 
        CardComponent,
        GeneralInformationComponent,
        TableComponent,
        LoaderComponent
    ]
})
export class SharedModule {}