export * from "./card/card.component";
export * from "./general-information/general-information.component";
export * from "./loader/loader.component";
export * from "./nav/nav.component";
export * from "./table/table.component";