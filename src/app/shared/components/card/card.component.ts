import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() 
  theme: string = 'default';

  @Input()
  amount: number = 0;

  @Input()
  accountNumber: string = ''

  constructor() { }

  ngOnInit(): void {}


  get lastDigits() {

    return this.accountNumber.substr(-4);
  }
}
