import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-general-information',
    templateUrl: './general-information.component.html',
    styleUrls: ['./general-information.component.scss']
})
export class  GeneralInformationComponent{    

    @Input()
    balance: number = 0;
}