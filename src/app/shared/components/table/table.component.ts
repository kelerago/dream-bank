import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { TableField } from "../../models";
import { environment } from "src/environments/environment";
import { PageEvent } from "@angular/material/paginator";

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit{

    constructor() {}

    @Input()
    data: object[] = []

    @Input()
    fields: TableField[] = [];

    @Input()
    pagination: boolean = false;

    @Input()
    dataLength: number = 0;
    
    @Output()
    detail: EventEmitter<string> = new EventEmitter();

    @Output()
    currentPage: EventEmitter<number> = new EventEmitter();

    columnV: string[] = []

    pageSize: number = environment.pageSize;

    ngOnInit(){

        this.fields.forEach((field:any) => {
            this.columnV.push(field.value);
        })
    }

    getDetail(entityID: string) {        

        this.detail.emit(entityID);
    }

    page(page: PageEvent) {

        this.currentPage.emit(page.pageIndex);
    }
}