import { AccountModule } from "../../account.module";
import { AccountService } from "./account.service";
import { TestBed } from '@angular/core/testing';
import { Account } from "../../models";

describe('AccountService', () => {    

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AccountModule]
        });        
      });

    it('get Balance #1', () => {

        const service: AccountService = TestBed.inject(AccountService);

        let accounts: Account[] = [
            {
                currency: 'COP',
                name: 'test',
                type: 'CASH',
                mainBalance: 10000
            },
            {
                currency: 'COP',
                name: 'test',
                type: 'CASH',
                mainBalance: 20000
            },
            {
                currency: 'COP',
                name: 'test',
                type: 'CASH',
                mainBalance: 30000
            }
        ]
    
        let balance = service.getBalance(accounts);
            
        expect(balance).toEqual(20000);
    });
    
    it('get Balance #2', () => {

        const service: AccountService = TestBed.inject(AccountService);

        let accounts: Account[] = [
            {
                currency: 'COP',
                name: 'test',
                type: 'CASH',
                mainBalance: 170000
            },
            {
                currency: 'COP',
                name: 'test',
                type: 'CASH',
                mainBalance: 90000
            },
            {
                currency: 'COP',
                name: 'test',
                type: 'CASH',
                mainBalance: 10000
            }
        ]
    
        let balance = service.getBalance(accounts);
            
        expect(balance).toEqual(90000);
    });
});