import { Injectable } from "@angular/core";
import { ApplicationHttpClient } from "src/app/shared/services/application-http-client.service";
import { environment } from "src/environments/environment";
import { Account } from "../../models";

@Injectable()
export class AccountService {

    constructor(private httpClient: ApplicationHttpClient){}

    public index() {

        return this.httpClient.Get(`/customers/${environment.clientID}/accounts`);
    }

    public getBalance(accounts: Account[]) {

        let response;

        if(accounts.length > 0){

            let balance: number = 0;
    
            accounts.forEach(account => {
    
                if(account.mainBalance) {
    
                    balance += account.mainBalance;
                }
            });
    
            balance = balance / accounts.length;
    
            response = parseFloat(balance.toFixed(3));

        } else {

            response = 0;
        }

        return response;
    }

    public store(account: Account) {        

        const bankAccount = this.generateBankAccount();

        let additionalData = {
            ...bankAccount,
            bic: "MOCKDEXXXXX",
            maskedPan: "12******3",
            pan: "120000003",
            status: "enabled"
        }
        
        Object.assign(account, additionalData);

        console.log(account);

        return this.httpClient.Post(`/customers/${ environment.clientID }/accounts`, account)       
    }

    private generateBankAccount()
    {
        
        let ktnr, iban;
        let pruef, pruef2;

        ktnr = (Math.round(Math.random() * 8999999) + 1000000);
        pruef = ((ktnr * 1000000) + 43);
        pruef2 = pruef % 97;
        pruef = 98 - pruef2;
        if (pruef > 9)
        {
        iban = "DE";
        }
        else
        {
        iban = "DE0";
        }

        iban = iban + pruef + "70050000" + "000" + ktnr;
            
        return {
            iban,
            bban: iban.substr(4)
        }        
    }
}