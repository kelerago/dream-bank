export interface Account {
    type: string,
    name: string,
    status?: string,
    currency: string,
    mainBalance?: number,
    iban?: string,
    bban?: string,
    bic?: string,
    pan?:string,
    maskedPan?: string
}