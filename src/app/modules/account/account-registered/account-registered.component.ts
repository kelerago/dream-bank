import { Component } from "@angular/core";

@Component({
    selector: 'app-account-registered',
    templateUrl: './account-registered.component.html',
    styleUrls: ['./account-registered.component.scss']
})
export class AccountRegisteredComponent {}