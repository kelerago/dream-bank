import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountRegisterComponent } from './account-register/account-register.component';
import { AccountRegisteredComponent } from "./account-registered/account-registered.component";
import { SharedModule } from 'src/app/shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AccountService } from './services/account/account.service';


@NgModule({
  declarations: [
    DashboardComponent,
    AccountRegisterComponent,
    AccountRegisteredComponent
  ],
  imports: [
    CommonModule,
    SharedModule,    
    NgxChartsModule
  ],
  providers: [AccountService],
  exports: []
})
export class AccountModule { }
