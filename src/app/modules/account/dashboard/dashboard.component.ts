import { Component, OnInit } from '@angular/core';
import { TableField } from 'src/app/shared/models';
import { AccountService } from '../services/account/account.service';
import { Account } from "./../models";
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {  

  constructor(
    private accountService: AccountService,
    private router: Router
    ) {}

  fields: TableField[] = [
    {
      value: 'externalId',
      text: ''
    },
    {
      value: 'type',
      text: 'Type'
    }, 
    {
      value: 'name',
      text: 'Type'
    }, 
    {
      value: 'status',
      text: 'Type'
    }, 
    {
      value: 'currency',
      text: 'Type'
    }, 
    {
      value: 'mainBalance',
      text: 'Balance'
    }
  ];
  
  accounts: Account[] = []; 

  get cards() {

    return this.accounts.slice(0, 2);
  }

  get balance() {

    return this.accountService.getBalance(this.accounts);
  }

  chart: any = {
    view: [200, 190],
    isDoughnut: true,
    tooltipDisabled: true,
    colorScheme: {
      domain: ['#98B2F9', '#DFE7F1']
    },
    budgets: [
      {
        name: 'a',
        value: 70
      },
      {
        name: 'b',
        value: 30
      }
    ]
  }  

  detail(accountID: string) {

    this.router.navigate(['/accounts', accountID]);
  }

  ngOnInit(): void {

    this.accountService.index().subscribe((response:any) => {
      
      this.accounts = response.data;
    });    
  }

}
