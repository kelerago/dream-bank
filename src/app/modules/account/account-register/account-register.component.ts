import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account/account.service';
import { Account } from "./../models";
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-account-register',
  templateUrl: './account-register.component.html',
  styleUrls: ['./account-register.component.scss']
})
export class AccountRegisterComponent implements OnInit {

  constructor(
    private accountService: AccountService,
    private router: Router,
    private notificationService: NotificationService
    ){}

  product: Account = {
    type: '',
    name: '',
    currency: ''
  };

  ngOnInit(): void {
  }

  register(){

    this.accountService.store(this.product)
    .subscribe(
      (data) => {

        this.router.navigate(['/account-registered']);
      },
      () => {
        this.notificationService.error('Invalid Data');
      }
    )
  }

}