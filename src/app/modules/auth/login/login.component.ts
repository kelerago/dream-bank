import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AuthData } from '../models/auth-data.model';
import { AuthResponse } from '../models/auth-response.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authData: AuthData = {
    username: '',
    password: '',
    grant_type: 'password'
  };

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService,
    private router: Router
  ) { 

  }

  ngOnInit(): void {
  }

  login() {

    this.authService.login(this.authData)
    .subscribe(
      (data: AuthResponse) => {
      
        this.router.navigate(['/']);
      },
      (message: string) => {

        this.notificationService.error(message);
      }
    );
  }

}
