export interface AuthData {
    username: string,
    password: string,
    grant_type: string
}