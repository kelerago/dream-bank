import { Component, OnInit } from '@angular/core';
import { TableField } from 'src/app/shared/models';
import { Transaction, TransactionFilter } from '../models';
import { ActivatedRoute, Router } from '@angular/router';
import { TransactionService } from '../services/transaction/transaction.service';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {
  
  constructor(
    private route: ActivatedRoute,
    private transactionService: TransactionService,
    private router: Router
  ) { }

  accountID: string = '';  

  transactions: Transaction[] = [];

  fields: TableField[] = [
    {
      value: 'externalId',
      text: ''
    },
    {
      value: 'currency',
      text: 'Currency',
    },
    {
      value: 'amount',
      text: 'Amount',
    },
    {
      value: 'creditorName',
      text: 'Creditor',
    },
    {
      value: 'valueDate',
      text: 'Date',
    }
  ]

  transactionsLength: number = 0;

  filters: TransactionFilter = {
    dateFrom: '',
    dateTo: ''
  };

  get balance() {

    return this.transactionService.getBalance(this.transactions);
  }

  ngOnInit(): void {

    this.accountID = this.route.snapshot.paramMap.get('id')!;

    this.index(this.accountID);
  }
  
  index(accountID: string, page = 0, filters = {}) {

    this.transactionService.index(accountID, page, filters)
    .subscribe((response:any) => {
      
      if(response.data) {

        this.transactions = response.data;

      } else {

        this.transactions = [];
      }
  
      this.transactionsLength = response.paging.numberOfObjects;
    });
  }
  
  detail(transactionID: string){

    this.router.navigate(['/accounts/transactions', transactionID]);
  }

  paginate(page: number) {

    this.index(this.accountID, page, this.filters);
  }

  filterTransactions() {

    this.index(this.accountID, 0, this.filters);
  }

}
