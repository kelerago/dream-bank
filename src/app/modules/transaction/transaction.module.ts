import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionDetailComponent } from './transaction-detail/transaction-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TransactionService } from './services/transaction/transaction.service';



@NgModule({
  declarations: [
    TransactionListComponent,
    TransactionDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [TransactionService]
})
export class TransactionModule { }
