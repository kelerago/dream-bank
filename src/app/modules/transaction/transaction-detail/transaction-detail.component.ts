import { Component, OnInit } from '@angular/core';
import { Transaction } from '../models';
import { ActivatedRoute } from '@angular/router';
import { TransactionService } from '../services/transaction/transaction.service';

@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss']
})
export class TransactionDetailComponent implements OnInit {

  transaction: Transaction = {
    amount: 0,
    creditorName: '',
    currency: '',
    valueDate: ''
  }

  constructor(
    private route: ActivatedRoute,
    private transactionService: TransactionService
  ) { }

  ngOnInit(): void {

    let transactionID = this.route.snapshot.paramMap.get('id');

    this.transactionService.detail(transactionID!)
    .subscribe((response:any) => {

      this.transaction = response;
    });
  }

}
