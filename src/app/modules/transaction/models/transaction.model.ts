export interface Transaction {
    valueDate: string,
    currency: string,
    amount: number,
    creditorName: string
}