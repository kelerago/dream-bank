export interface TransactionFilter {
    dateFrom: string,
    dateTo: string
}