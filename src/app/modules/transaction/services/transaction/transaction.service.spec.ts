import { TransactionModule } from "../../transaction.module";
import { TransactionService } from "./transaction.service";
import { Transaction } from "../../models";
import { TestBed } from '@angular/core/testing';

describe('TransactionService', () => {    

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [TransactionModule]
        });        
    });

    it('get Balance #1', () => {

        const service: TransactionService = TestBed.inject(TransactionService);

        let transactions: Transaction[] = [
            {
                valueDate: '2021-01-01',
                currency: 'COP',
                amount: 10000,
                creditorName: 'Falabella'
            },
            {
                valueDate: '2021-01-01',
                currency: 'COP',
                amount: 10000,
                creditorName: 'Falabella'
            },
            {
                valueDate: '2021-01-01',
                currency: 'COP',
                amount: 10000,
                creditorName: 'Falabella'
            }
        ]
    
        let balance = service.getBalance(transactions);
            
        expect(balance).toEqual(10000);
    });
    
    it('get Balance #2', () => {

        const service: TransactionService = TestBed.inject(TransactionService);

        let transactions: Transaction[] = [
            {
                valueDate: '2021-01-01',
                currency: 'COP',
                amount: 35000,
                creditorName: 'Falabella'
            },
            {
                valueDate: '2021-01-01',
                currency: 'COP',
                amount: 35000,
                creditorName: 'Falabella'
            },
            {
                valueDate: '2021-01-01',
                currency: 'COP',
                amount: 20000,
                creditorName: 'Falabella'
            }
        ]
    
        let balance = service.getBalance(transactions);
            
        expect(balance).toEqual(30000);
    });
});