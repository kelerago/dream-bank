import { HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ApplicationHttpClient } from "src/app/shared/services/application-http-client.service";
import { environment } from "src/environments/environment";
import { Transaction } from "../../models";

@Injectable()
export class TransactionService {

    constructor(
        private httpClient: ApplicationHttpClient
    ) {}

    index(accountID: string, page: number, filters = {}) {

        console.log(filters);

        let params = new HttpParams().set('accountId', accountID)
                                     .set('pageSize', environment.pageSize.toString())
                                     .set('page', page.toString());

        Object.keys(filters).forEach(filter => {

            // @ts-ignore
            let data = filters[filter];            

            if(typeof data == "object"){

                data = data.toISOString().split('T')[0];
            }

            // @ts-ignore
            params = params.set(filter, data);
        })

        return this.httpClient.Get(`/customers/${ environment.clientID }/transactions/`, { params });
    }

    detail(transactionID: string) {        

        return this.httpClient.Get(`/customers/${ environment.clientID }/transactions/${ transactionID }`);
    }

    public getBalance(transactions: Transaction[]) {

        let response;

        if(transactions.length > 0){

            let balance: number = 0;
    
            transactions.forEach(transaction => {
    
                if(transaction.amount) {
    
                    balance += transaction.amount;
                }
            });
    
            balance = balance / transactions.length;
    
            response = parseFloat(balance.toFixed(3));

        } else {

            response = 0;
        }

        return response;
    }
}