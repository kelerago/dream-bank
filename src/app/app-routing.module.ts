import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountRegisterComponent } from './modules/account/account-register/account-register.component';
import { AccountRegisteredComponent } from './modules/account/account-registered/account-registered.component';
import { DashboardComponent } from './modules/account/dashboard/dashboard.component';
import { LoginComponent } from './modules/auth/login/login.component';
import { TransactionDetailComponent } from './modules/transaction/transaction-detail/transaction-detail.component';
import { TransactionListComponent } from './modules/transaction/transaction-list/transaction-list.component';
import { AuthGuard, NoAuthGuard } from './shared/guards';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: '',    
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'register-account',
        component: AccountRegisterComponent,
      },
      {
        path: 'account-registered',
        component: AccountRegisteredComponent
      },
      {
        path: 'accounts/:id',
        component: TransactionListComponent
      },
      {
        path: 'accounts/transactions/:id',
        component: TransactionDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
