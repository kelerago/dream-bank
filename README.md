# Dream Bank

Remember to download dependencies with yarn or npm

## Folder Structure

### src/app/modules
In this folder is stored all screen all feature components

### src/app/shared
In this folder is stored reusable components, services and styles in order to centralize the code.